if (!Array.prototype.indexOf)
	{
Array.prototype.indexOf = function(obj, start) {
     for (var i = (start || 0), j = this.length; i < j; i++) {
         if (this[i] === obj) { return i; }
     }
     return -1;
	}
	}

function Nodo(id,x,y,text,detail)
{ //passo id del Nodo, coordinate del centro e id dei nodi a cui č collegato
	this.id=id;
	this.x=x; 
	this.y=y;
	this.indexLista=-1;
	this.text=text;
	this.detail=detail;
	this.getDetail=getDetail;
	this.amici= new Array(); //elenco degli id amici del nodo		
	this.getFriend=getFriend; //funzione per ottenere un id dell'amico in una posizione
	this.getId=getId; //funzione per ottenere l'id di questo nodo
	this.getFriendsNumber=getFriendsNumber; //funzione per ottenere il numero di nodi amici di questo nodo
	this.setCentre=setCentre; //funzione per settare le coordinate x,y del nodo
	this.getX=getX; //getter della coordinata X del nodo
	this.getY=getY;//getter della coordinate Y del nodo
	this.highLightsNode=highLightsNode; //evidenzia gli archi uscenti da questo nodo
	this.getText=getText;
	this.addFriend=addFriend;
	this.getIndexLista= getIndexLista;
	this.setIndexLista=setIndexLista;
	this.getBetweenness=getBetweenness;
	this.getFriendsIds=getFriendsIds;
	function getDetail() { return this.detail; }
	function getFriendsIds(){return this.amici;}
	function setIndexLista(i) {this.indexLista=i; }
	function getIndexLista() { return this.indexLista; }
	function getX()	{return this.x;	}
	function getY()	{return this.y; }
	function getText() {return this.text;}
	function setCentre(x,y){
		this.x=x;
		this.y=y;
	}
	function getFriendsNumber(){return this.amici.length;}
	function getId(){return id;}
	function getFriend(n){return this.amici[n];}
	function getBetweenness()
	{
		betweenness=0;
		for(i=0;i<this.amici.length-1;i++)
		{
			idAmiciIesimoAmico=nodi[this.amici[i]].getFriendsIds();
			for(j=i+1;j<this.amici.length;j++)
			{
				//Se l'amico i-esimo ha tra gli amici il j-esimo allora sono collegati direttamente e non mi interessa ai
				//fini della betweenness
				if (idAmiciIesimoAmico.indexOf(this.amici[j])>=0)
					break;
				else
				{
					idAmiciJesimoAmico=nodi[this.amici[j]].getFriendsIds();
					var conteggioAmiciInComune=0;
					//Conto gli amici in comune tra i-esimo e j-esimo
					for(x=0;x<idAmiciIesimoAmico.length;x++)
						if (idAmiciJesimoAmico.indexOf(idAmiciIesimoAmico[x])>=0)
							conteggioAmiciInComune++;
					//Incremento la betweenness basandomi sul numero degli amici in comune
					betweenness+=1/conteggioAmiciInComune;
				}
			}
		}
		return betweenness;
	}
	function highLightsNode(context)
	{
		context.fillRect(10,10,1000,800);
		context.clearRect(20,20,970,770); //clear dell'oggetto Canvas
		for (j=0; j<nodi.length; j++) //esamino tutti i nodi
		{
			for (i=0; i<nodi[j].getFriendsNumber(); i++) //per ogni nodo valuto tutti gli archi uscenti
			{
				context.beginPath(); //inizio a disegnare
				context.strokeStyle="Blue"; //imposto il Blue come colore di default
				if (nodi[j].getId()==this.id || nodi[nodi[j].getFriend(i)].getId()==this.id)
				{
					context.strokeStyle="Red"; //se il nodo č quello cliccato evidenzio in rosso
				}
				context.moveTo(nodi[j].getX(),nodi[j].getY()); //punto di partenza
				context.lineTo(nodi[nodi[j].getFriend(i)].getX(),nodi[nodi[j].getFriend(i)].getY()); // punto di arrivo della linea
				context.stroke(); //disegno fisicamente sul canvas
			}
		}
	}
	function addFriend(id)
	{
		if (this.amici.indexOf(id)<0)
		this.amici[this.amici.length]=id;
	}
}
function disegnaNodo(k,id,text,detail, nodoRif)
{ //disegna il k° nodo
	k=nodi.length;
	for (i=0; i<k; i++)
	{
		if (nodi[i].getId()==id)
			return i;
	}
	var NUM_CIRC=4;
	var raggio=(k % NUM_CIRC)*(10+Math.random()*20)+100; // raggio minimo=200; raggio max=450;
	var delta_phi=Math.random()*2*Math.PI;
	var phi=Math.random()*10*delta_phi;
	if (nodoRif==null)
	{
		myX=Math.round(500+raggio*Math.cos(phi))+Math.random()*30; // myX min =70; myX max =930;
		myY=Math.round(350+raggio*Math.sin(phi))+Math.random()*30; // myY min=70; myY max = 930;
	}else
	{
		
		xRif=nodoRif.getX();
		yRif=nodoRif.getY();
		//distanza centro- nodo di riferimento
		var dist= Math.sqrt((xRif-500)*(xRif-500)+(yRif-350)*(yRif-350));
		// angolo del nodo di riferimento: sen alfa = deltaY/r -> alfa = arcsen (deltaY/r)
		var phiRif=Math.asin((yRif-350)/dist);
		if (((xRif-500)/dist)<0)
		{
				phiRif=Math.PI-phiRif;
		}
		delta_phi=Math.random()*Math.PI/2.5-Math.PI/5;
		raggio= dist+Math.random()*50;
		myX=Math.round(xRif+raggio*Math.cos(phiRif+delta_phi))+Math.random()*30;
		myY=Math.round(yRif+raggio*Math.sin(phiRif+delta_phi))+Math.random()*30;
	}
	
	nodi[k]=new Nodo(id,myX,myY,text,detail);
	var contenitore= document.getElementById("grafo"); //div contenitore
	var nodo= document.createElement("div"); //creo l'elemento fisico del nodo
	nodo.setAttribute("style","width: 60px; height: 25px; background-color: cyan; position: absolute; left: "+(nodi[k].getX()-12)+"px; top: "+(nodi[k].getY()-12)+"px;" ); //stile del nodo
	nodo.setAttribute("id",k); //imposto l'id
	nodo.setAttribute("draggable","true"); //proprietŕ draggable a true
	nodo.innerHTML=text; //immagine
	if (!nodo.addEventListener)
	{
		nodo.attachEvent("onclick",nodoCliccato);
		nodo.attachEvent("ondblclick",nodoDoubleCliccato);
		nodo.attachEvent("dragover",dragOver);
	
	}else
	{
		nodo.addEventListener("click", nodoCliccato,false); 
		nodo.addEventListener("dblclick", nodoDoubleCliccato,false);
		nodo.addEventListener("dragover",function ( event ) { reply_dragOver(event, this.id, context); },false);
	}
	/* eventi */
	function dragOver(){reply_dragOver(event,this.id,context);}
	function nodoDoubleCliccato(){reply_double_click(this.id);}
	function nodoCliccato(){reply_click(this.id,context);}
	
	function reply_dragOver(event,id,context)
	{
		if (event.clientX!=0 && event.clientY!=0)
		{
			nodi[id].setCentre(event.clientX,document.body.scrollTop+event.clientY); //setto il nuovo centro
			var nodo=document.getElementById(id); //recupero il nodo corretto
			nodo.setAttribute("style","width: 260x; height: 25px;background-color:cyan; position: absolute; left: "+(event.clientX-12)+"px; top: "+(document.body.scrollTop+event.clientY-12)+"px;"); //modifico la posizione
			nodi[id].highLightsNode(context); //evidenzio il nodo
		}
	}
	function reply_double_click(id,context)
	{
		if (document.getElementById("mostraInfo")==null)
		{
			var mostraInfo=document.createElement("div"); //div contenitore delle info
			mostraInfo.setAttribute("style","position: absolute; left:"+nodi[id].getX()+"px; top: "+nodi[id].getY()+"px; width:100px; height:100px; background-color:red; ");
			mostraInfo.setAttribute("id","mostraInfo");
			/*var closeMostraInfo = document.createElement("span");
			closeMostraInfo.setAttribute("style","position: relative; left: 275px; top:0px; width: 20px; height: 20px; cursor:pointer;");
			closeMostraInfo.innerHTML="X";
			mostraInfo.appendChild(closeMostraInfo);
			*/
			contenitore.appendChild(mostraInfo);
			mostraInfo.innerHTML= '<span class="glyphicon glyphicon-remove"style="position: relative; left: 85px; top:0px; width: 20px; height: 20px; cursor:pointer;" onClick="chiudiMostraInfo()">X</span><br>'+nodi[id].getDetail();
		} else
		{
			chiudiMostraInfo();
		}
	}
	function reply_click(id,context)
	{ //evidenzio il nodo
		nodi[id].highLightsNode(context);
		
	}
	contenitore.appendChild(nodo);
	return k;
}

function chiudiMostraInfo()
{ //funzione per eliminare il div delle informazioni
	document.getElementById("grafo").removeChild(document.getElementById("mostraInfo"));
}